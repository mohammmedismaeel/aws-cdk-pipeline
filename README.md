# Creating a CI/CD pipeline using AWS CDK with bitbucket!!!

## Creating bitbucket repo and react upp

Create a repo on bitbucket and clone it to your pc
Create a React App in the same folder [Create React App](https://github.com/facebook/create-react-app).

```bash
npx create-react-app my-app
cd my-app
yarn start
```

Push all changes to master branch

# Getting Started AWS CDK

Install AWS CDK

```bash
# Using NPM
$ > npm install -g aws-cdk
# Using Yarn
$ > yarn global add aws-cdk
```

### Configure AWS CLI

Generate an `Access Key` and `Secret Access Key` for your AWS account.

```bash
$ > export AWS_ACCESS_KEY_ID="…"
$ > export AWS_SECRET_ACCESS_KEY="…"
$ > export AWS_SESSION_TOKEN="…"
```

# Connect AWS pipeline to bitbucket

We will need the Connection ARN [Connect AWS pipeline to bitbucket](https://docs.aws.amazon.com/dtconsole/latest/userguide/connections-create-bitbucket.html)
Change it with the one in the /infr/lib/infra-stack.ts

Create a new folder outside the src folder
Navigate to the new folder cd new folder
Init the CDK project

```bash
cdk init myApp --language typescript
```

## Deploy your infrastructure to AWS

```bash
cdk deploy
```

If everything works fine the pipeline will be trigger every time you check in a new code
You can define deferent stages in the pipeline

- dev
- staging
- prod

You can use S3 to host your site for more information check my github project [AWS CDK pipeline with Github action](https://github.com/Mohammedismaeell/ReactPipelineCDK)
