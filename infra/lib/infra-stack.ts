import * as CDK from "@aws-cdk/core";
import * as CodeBuild from "@aws-cdk/aws-codebuild";
import * as CodePipeline from "@aws-cdk/aws-codepipeline";
import * as CodePipelineAction from "@aws-cdk/aws-codepipeline-actions";
import * as S3 from "@aws-cdk/aws-s3";

export class InfraStack extends CDK.Stack {
  constructor(scope: CDK.App, id: string, props: any) {
    super(scope, id, props);

    // AWS CodeBuild artifacts
    const outputSources = new CodePipeline.Artifact();
    const outputWebsite = new CodePipeline.Artifact();

    // AWS CodePipeline pipeline
    const pipeline = new CodePipeline.Pipeline(this, "Pipeline", {
      pipelineName: "MyWebsite",
      restartExecutionOnUpdate: true,
    });

    // AWS CodePipeline stage to clone sources from bitbucket repository
    pipeline.addStage({
      stageName: "Source",
      actions: [
        new CodePipelineAction.CodeStarConnectionsSourceAction({
          actionName: "Checkout",
          owner: "mohammmedismaeel",
          repo: "pipelinetest",
          output: outputSources,
          branch: "master",
          connectionArn:
            "arn:aws:codestar-connections:eu-north-1:436222975604:connection/66914dce-704b-4dbc-8610-47e69f46c3b8",
        }),
      ],
    });

    // AWS CodePipeline stage to build CRA website and CDK resources
    pipeline.addStage({
      stageName: "Build",
      actions: [
        // AWS CodePipeline action to run CodeBuild project
        new CodePipelineAction.CodeBuildAction({
          actionName: "Website",
          project: new CodeBuild.PipelineProject(this, "BuildWebsite", {
            projectName: "MyWebsite",
            buildSpec: CodeBuild.BuildSpec.fromSourceFilename(
              "./infra/lib/build.yml"
            ),
          }),
          input: outputSources,
          outputs: [outputWebsite],
        }),
      ],
    });

    const bucketWebsite = new S3.Bucket(this, "Files", {
      websiteIndexDocument: "index.html",
      websiteErrorDocument: "error.html",
      publicReadAccess: true,
    });

    // AWS CodePipeline stage to deploy website and CDK resources
    pipeline.addStage({
      stageName: "Deploy",
      actions: [
        // AWS CodePipeline action to deploy website to S3
        new CodePipelineAction.S3DeployAction({
          actionName: "Website",
          input: outputWebsite,
          bucket: bucketWebsite,
        }),
      ],
    });
  }
}
